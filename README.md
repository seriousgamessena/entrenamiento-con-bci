# Serious Games
## Entrenamiento con BCI
### Synopsis

En la parte superior del archivo debe haber una breve introducción y / o resumen que explica ** qué ** es el proyecto.

### Motivación

Una breve descripción de la motivación detrás de la creación y el mantenimiento del proyecto. Esto debería explicar ** por qué ** el proyecto existe.

## Empezando



### Requisitos previos

Qué cosas necesitas para instalar el software y cómo instalarlas

```
Dar ejemplos
```

### Instalación

Una serie de ejemplos paso a paso que le indican cómo obtener un entorno de desarrollo en ejecución

Di lo que será el paso

```
Da el ejemplo
```

Y repetir

```
hasta que termine
```

Termine con un ejemplo de cómo sacar algunos datos del sistema o usarlos para una pequeña demostración

## Cómo contribuir

1. Clone el repositorio
2. Cree el branch con el nuevo feature (`git checkout -b feature/elNuevoFeature`)
3. Haga un commit con sus cambios (`git commit -am 'Añade el nuevo feature'`)
4. Haga un Push en su branch (`git push origin feature/fooBar`)
5. Cree un nuevo Pull Request

## Release History

* 0.0.1
    * Work in progress

## Autores

* **Alejandro Diaz** - *Programador Lider* - [Aldiazj](https://github.com/aldiazj)

## Creditos
  *  Eddy Santiago Zapata Higuita -Programador- [Eddyszh](https://github.com/Eddyszh)
  *  Santiago Cardona Arango -Programador- [scasantiago](https://github.com/scasantiago06)
  *  Sebastian Pacheco Bulla -Programador- [sebaspb](https://github.com/sebaspb)
  *  Mauricio Alberto Ramirez Lopez -Programador- [MauricioRL7](https://github.com/MauricioRL7)
  *  Emanuel Restrepo -Programador- [E-restrepo14](https://github.com/E-restrepo14)
  *  Kevin Smith Jimenez Garro -Programador- [kevin4809](https://github.com/kevin4809)
  *  Sergio Andres Mejia Cordoba -Programador- [Serandrus](https://github.com/Serandrus)
  *  Andersen Damian Castañeda Cardenas -Programador- [AndersenCastaneda](https://github.com/AndersenCastaneda)
  *  Lobsang Sanchez Henao -Programador- [ModernWestern](https://github.com/modernwestern)
  *  Yency Yanila Padilla Guizamano -Programador- [YencyPadilla](https://github.com/yencypadilla)
  *  Jorge Luis Suarique Agudelo -Programador- [JorgeLuisSuarique](https://github.com/JorgeLuisSuarique)
  *  Abdel Baruc Mejia Camargo -Programador- [Bhalut](https://github.com/Bhalut)
  *  Nahed David Alvarez Beltran -Programador- [NahedDavidAlvarezBeltran](https://github.com/NahedDavidAlvarezBeltran)
  *  Luis Fernando Torres Aguirre -Programador- []()
  *  Dylan Alexis Ramirez Meneses -Programador- []()
## Licencia

Establezca qué tipo de licencia maneja el proyecto

## Acknowledgments

* Menciona cualquier persona cuyo código fue utilizado
* O que haya servido de inspiración
* etc